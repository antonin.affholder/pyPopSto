#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as scint
from scipy import stats
from itertools import chain, zip_longest

# Just trying the bdf for a simple verhulst
def bdf_kernel(n,b,d,dt):
    """
    Discrete time jump in population size using the so called BDF from the ZEN manual
    *arguments*
    - n  : int   population size
    - b  : float per capita birth rate
    - d  : float per capita death rate
    - dt : float time interval of discretization
    *ouputs*
    - s  : new population size
    """
    #convention is that the rates are given per capita
    b = b*n
    d = d*n
    r = b-d
    if r<0:
        p0  = d*(1-np.exp(r*dt))/(d-b*np.exp(r*dt))
        rho = (d-b)/(d-b*np.exp(r*dt))
    elif r==0:
        p0  = b*dt/(1+b*dt)
        rho = 1/(1+b*dt)
    else:
        p0  = d*(1-np.exp(-r*dt))/(b-d*np.exp(-r*dt))
        rho = (b-d)*np.exp(-r*dt)/(b-d*np.exp(-r*dt))
    #s = 0
    #for i in range(n):
    #    if np.random.random()>p0:
    #        s+=np.random.geometric(rho)
    s = np.sum(np.random.geometric(rho,size=np.sum(np.random.random(size=n)>p0)))
    return(s)

def Poisson_kernel(n,b,d,dt):
    """
    Discrete time jump in population size using the Poisson approximation of growth
    *arguments*
    - n  : int   population size
    - b  : float per capita birth rate
    - d  : float per capita death rate
    - dt : float time interval of discretization
    *ouputs*
    - s  : new population size
    """
    r = (b-d)*n
    return(np.random.poisson(n*np.exp(r*dt)))

def Tau_leap_kernel(n,b,d,dt):
    """
    Discrete time jump in population size using the Tau-Leap algorithm
    *arguments*
    - n  : int   population size
    - b  : float per capita birth rate
    - d  : float per capita death rate
    - dt : float time interval of discretization
    *ouputs*
    - s  : new population size
    """
    B = np.random.poisson(n*b*dt)
    D = np.random.poisson(n*d*dt)
    return(np.clip(n+B-D,0,np.infty))

def Tau_leap_BDkernel(nvec,b,d,dt):
    """
    Discrete time jump in population size using the Tau-Leap algorithm
    *arguments*
    - n  : int   population size
    - b  : float per capita birth rate
    - d  : float per capita death rate
    - dt : float time interval of discretization
    *ouputs*
    - s  : new population size
    """
    B = np.random.poisson(nvec*b*dt)
    D = np.random.poisson(nvec*d*dt)
    return(B,D)



# Kernels for the continuous non stochastic variable (ie nutrient concentration)



def Euler_kernel(nn,n,c,dfun,dfun_pars,dt):
    """
    Discrete time jump in nutrient concentration using a simple forward Euler scheme
    *arguments*
    - nn        : int   population size at t+dt (keeping it for consistency)
    - n         : int   population size at t
    - c         : float nutrient concentration at t
    - dfun      : func  function for the time derivative of c. signature dfun(n,c,**dfun_pars) -> dc/dt
    - dfun_pars : dict  parameters of dfun
    - dt        : float time interval of discretization
    *ouputs*
    - nc        : new nutrient concentration at t+dt
    """
    nc = c+dt*dfun(n,c,**dfun_pars)
    return(np.clip(nc,0,np.infty))

def MidEuler_kernel(nn,n,c,dfun,dfun_pars,dt):
    """
    Discrete time jump in nutrient concentration using a simple Euler assuming n(t+dt/2)
    *arguments*
    - nn        : int   population size at t+dt (keeping it for consistency)
    - n         : int   population size at t
    - c         : float nutrient concentration at t
    - dfun      : func  function for the time derivative of c. signature dfun(n,c,**dfun_pars) -> dc/dt
    - dfun_pars : dict  parameters of dfun
    - dt        : float time interval of discretization
    *ouputs*
    - nc        : new nutrient concentration at t+dt
    """
    nm = (n+nn)/2
    nc = c+dt*dfun(nm,c,**dfun_pars)
    return(np.clip(nc,0,np.infty))

def RK2_kernel(nn,n,c,dfun,dfun_pars,dt):
    """
    Discrete time jump in nutrient concentration using a sort of Runge-Kutta scheme
    *arguments*
    - nn        : int   population size at t+dt (keeping it for consistency)
    - n         : int   population size at t
    - c         : float nutrient concentration at t
    - dfun      : func  function for the time derivative of c. signature dfun(n,c,**dfun_pars) -> dc/dt
    - dfun_pars : dict  parameters of dfun
    - dt        : float time interval of discretization
    *ouputs*
    - nc        : new nutrient concentration at t+dt
    """
    nm = (n+nn)/2
    cm = c+(dt/2)*dfun(n,c,**dfun_pars)
    nc = c+dt*dfun(nm,cm,**dfun_pars)
    return(np.clip(nc,0,np.infty))


def PMRK2_kernel(nnvec,nvec,c,pvec,dfun,dfun_pars,dt):
    # This is just the same as the RK2 continuous kernel???
    nmvec = (nvec+nnvec)/2
    cm    = c+(dt/2)*dfun(nvec,c,pvec,**dfun_pars)
    nc    = c+dt*dfun(nmvec,cm,pvec,**dfun_pars)
    return(np.clip(nc,0,np.infty))


# Mutation kernels

def continuoustrait_mutation_kernel(N,B,pop_phenotype,mu,sig,p_range=[0,1]):
    """
    N : array of int population size in each population
    B : array of int number of offspring per population
    pop_phenotype : array of float values of the phenotype in the populations
    mu : float mutation rate (probability that the offspring is a mutant)
    sig : float typical effect of the mutation on the phenotype
    p_range : range of values in which the phenotype is taken
    
    This outputs alsot the phenotype of parents because I expect this to be useful for further modeling
    What would also be easy to get, is the "rosetta stone" that links daughter pops with parents pops
    which could also be useful to get competition locally between the parent and the daughter pops
    in order to encompass local competition not just due to phenotype differences but also density differences
    """
    # first the number of mutants
    nmuts = np.random.binomial(B,mu)
    # total number of mutants
    nm = np.sum(nmuts)
    
    if nm == 0:
        N_prime = np.copy(N)
        p_phen  = np.copy(pop_phenotype)
        d_phen  = np.copy(pop_phenotype)
    else:
        # number of empty slots in the metapop array
        nem = np.sum(N==0)

        # Prepare the new population affected by mutations
        N_prime = np.copy(N) # using copy to avoid mutability

        # Indexes where to add the mutants NOTE THAT IF nm>nem, some mutants are just ignored and lost from the population
        mut_index = np.where(N==0)[0][:np.min([nm,nem])]

        # Add +1 pop in empty slots filled with a mutant
        N_prime[mut_index] = 1

        # Remove mutants from their parent pop
        N_prime = N_prime - nmuts

        # Parent Indexes that have mutants
        pmidx = np.where(nmuts)[0]

        # Number of mutants for these indexes
        nmidx = nmuts[pmidx]

        # list from which to sample parents of mutants
        ipar = list(chain.from_iterable([[pmidx[k]]*nmidx[k] for k in range(np.sum(nmuts>0))]))

        # Sampling parent index for mutants
        mparidx = np.random.choice(ipar,size=len(mut_index),replace=False)

        # Index of non-mutated and divided
        nmparidx = np.where(N>0)[0]
        
        # Index of populations where no divisions occured - they keep the same parental population - not actually useful
        # nodividx = np.where(B==0)[0]

        # The Rosetta stone between the original population and the population with mutants, who comes from who ?
        # First, initiate with the same parental
        gen_tr_idx = np.array([np.nan]*len(N_prime))
        # Then, fill the parental of mutants
        gen_tr_idx[mut_index] = mparidx
        # Then, fill the parental of non-mutating, here, if there have been division, that might change the parental 
        gen_tr_idx[nmparidx] = nmparidx
        # If there has not been a division, then the parental remains the same parental as at the previous step 

        # Parent phenotype of the new population
        p_phen = pop_phenotype[np.array(gen_tr_idx,dtype=int)]

        # Preparing the daugther population phenotype
        d_phen = np.copy(p_phen)

        # Sample the mutant phenotype, clipping to p_range
        mphen = np.clip(np.random.normal(p_phen[mparidx],sig),*p_range)

        # Change the daughter metapop phenotype
        d_phen[mut_index] = mphen
    
    # Returns the mutation affected metapop, the phenotype of the parent of each of these pops and the phenotype of this pop
    return(N_prime,p_phen,d_phen) # Should be good ??




def microbial_dvlpt_mutK(N,B,pop_trait,pop_phenotype,mu,sig,x_range=[0,1]):
    """
    This kernel should account for a trait mutation that is not necessarily associated with a change in phenotype.
    Hence, the mutant inherits the parental phenotype but has a new trait value
    N : array of int population size in each population
    B : array of int number of offspring per population
    pop_phenotype : array of float values of the phenotype in the populations
    mu : float mutation rate (probability that the offspring is a mutant)
    sig : float typical effect of the mutation on the phenotype
    p_range : range of values in which the phenotype is taken
    
    This outputs alsot the phenotype of parents because I expect this to be useful for further modeling
    What would also be easy to get, is the "rosetta stone" that links daughter pops with parents pops
    which could also be useful to get competition locally between the parent and the daughter pops
    in order to encompass local competition not just due to phenotype differences but also density differences
    ^^^^ thanks smart Antonin from the past but that made the code so difficult to understand
    """
    # first the number of mutants
    nmuts = np.random.binomial(B,mu)
    # total number of mutants
    nm = np.sum(nmuts)
    
    if nm == 0: # No mutation
        N_prime  = np.copy(N)
        p_trait  = np.copy(pop_trait)
        p_pheno  = np.copy(pop_phenotype)
        d_trait  = np.copy(pop_trait)
        d_pheno  = np.copy(pop_phenotype)
        
    else:
        # number of empty slots in the metapop array
        nem = np.sum(N==0)

        # Prepare the new population affected by mutations
        N_prime = np.copy(N) # using copy to avoid mutability

        # Indexes where to add the mutants NOTE THAT IF nm>nem, some mutants are just ignored and lost from the population
        mut_index = np.where(N==0)[0][:np.min([nm,nem])]

        # Add +1 pop in empty slots filled with a mutant
        N_prime[mut_index] = 1

        # Remove mutants from their parent pop
        N_prime = N_prime - nmuts

        # Parent Indexes that have mutants
        pmidx = np.where(nmuts)[0]

        # Number of mutants for these indexes
        nmidx = nmuts[pmidx]

        # list from which to sample parents of mutants
        ipar = list(chain.from_iterable([[pmidx[k]]*nmidx[k] for k in range(np.sum(nmuts>0))]))

        # Sampling parent index for mutants
        mparidx = np.random.choice(ipar,size=len(mut_index),replace=False)

        # Index of non-mutated and divided
        nmparidx = np.where(N>0)[0]
        
        # Index of populations where no divisions occured - they keep the same parental population - not actually useful
        # nodividx = np.where(B==0)[0]

        # The Rosetta stone between the original population and the population with mutants, who comes from who ?
        # First, initiate with the same parental
        gen_tr_idx = np.array([np.nan]*len(N_prime))
        # Then, fill the parental of mutants
        gen_tr_idx[mut_index] = mparidx
        # Then, fill the parental of non-mutating, here, if there have been division, that might change the parental 
        gen_tr_idx[nmparidx] = nmparidx
        # If there has not been a division, then the parental remains the same parental as at the previous step 

        # Parental traits of the new population
        p_trait = pop_trait[np.array(gen_tr_idx,dtype=int)]
        
        # Parental phenotype of the new population
        p_phen = pop_phenotype[np.array(gen_tr_idx,dtype=int)]

        # Preparing the daugther population phenotype and trait
        d_phen  = np.copy(p_phen)
        d_trait = np.copy(p_trait)
        
        # Sample the mutant trait, clipping to p_range
        mtrait = np.clip(np.random.normal(p_trait[mparidx],sig),*x_range)

        # Change the daughter metapop phenotype
        d_trait[mut_index] = mtrait
    
    # Returns the mutation affected metapop, the phenotype of the parent of each of these pops and the phenotype of this pop
    return(N_prime,p_trait,p_phen,d_trait,d_phen) # Should be good ??




