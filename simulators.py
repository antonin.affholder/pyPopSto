#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as scint
from scipy import stats
from itertools import chain, zip_longest

from pyPop import transition_kernels as tker


def discrete_bdm(N0,ratefun_pc,kernel_fun,dt,tmax,rpars):
    """
    Discrete Birth death model implementation for an autonomous monomorphic population
     - N0 : int initial population size
     - ratefun_pc : vectorized function that returns birth and death *per capita* rates
     - kernel_fun : function that returns the new population given the old one and the rates
     - dt : float time interval
     - tmax : float max time
     - rpars : dict of keyword parameters to pass to the rate function
    """
    N = [N0]
    time = np.arange(dt,tmax,dt)
    for t in time[1:]:
        n = N[-1]
        B,D = ratefun_pc(n,**rpars)
        S = kernel_fun(n,B,D,dt)
        N.append(S)
    return(time,N)


def hybrid_dbdm(N0,C0,ratefun_pc,dconcfun,popkernel_fun,ckernel_fun,dt,tmax,rpars,cpars):
    """
    Hybrid birth-death simulation
    - N0 : int initial population size
    - C0 : float initial nutrient concentration
    - ratefun_pc : vectorized function that returns birth and death *per capita* rates
    - dconcfun : function that gives the derivative of the deterministic variable
    - popkernel_fun : function that returns the new population given the old one and the rates
    - ckernel_fun : deterministic kernel for the resource/continuous variable
    - dt : float time interval
    - tmax : float max time
    - rpars : dict of keyword parameters to pass to the rate function
    - cpars : dict of keyword parameters to pass to the resource rate function
    
    """
    N = [N0]
    C = [C0]
    time = np.arange(dt,tmax,dt)
    for t in time[1:]:
        n = N[-1]
        c = C[-1]
        B,D = ratefun_pc(n,c,**rpars)
        nn = popkernel_fun(n,B,D,dt)
        N.append(nn)
        nc = ckernel_fun(nn,n,c,dconcfun,cpars,dt)
        C.append(nc)
    return(time,np.array(N),np.array(C))



def poly_hybrid_dbdm(N0,P0,C0,ratefun_pc,dconcfun,BDpopkernel_fun,ckernel_fun,dt,tmax,rpars,cpars):
    # N0 is now a vector
    # P0 is the initial phenotype distribution
    # ratefun_pc needs to be adequately vectorized
    
    """
    Modified hybrid_dbdm to accomodate polymorphous metapopulation
    - N0 : list/array initial population size
    - C0 : float initial nutrient concentration
    - P0 : list/array initial phenotypes
    - ratefun_pc : vectorized function that returns birth and death *per capita* rates
    - dconcfun : function that gives the derivative of the deterministic variable
    - popkernel_fun : function that returns the new population given the old one and the rates
    - ckernel_fun : deterministic kernel for the resource/continuous variable
    - dt : float time interval
    - tmax : float max time
    - rpars : dict of keyword parameters to pass to the rate function
    - cpars : dict of keyword parameters to pass to the resource rate function
    
    """
    N = [N0]
    C = [C0]
    P = [P0]
    time = np.arange(dt,tmax,dt)
    for t in time[1:]:
        nvec = N[-1]
        c = C[-1]
        pvec = P[-1]
        b,d = ratefun_pc(nvec=nvec,c=c,x=pvec,pvec=pvec,**rpars) #operates serially on focal phenotype x in pvec, thus B and D are vectors
        B,D = BDpopkernel_fun(nvec,b,d,dt)
        nnvec = np.clip(nvec+B-D,0,np.infty)
        nc = ckernel_fun(nnvec,nvec,c,pvec,dconcfun,cpars,dt)
        npvec = pvec # Will change with mutations
        # Future me might add a skip parameter, as in the code by doulcier
        N.append(nnvec)
        C.append(nc) 
        P.append(npvec)
    return(time,np.array(N),np.array(C),np.array(P))



def evol_hybrid_dbdm(N0,P0,C0,ratefun_pc,dconcfun,BDpopkernel_fun,ckernel_fun,Mkernel,dt,tmax,rpars,cpars,Mpars):
    """
    Modified hybrid_dbdm to accomodate polymorphous metapopulation and a mutation kernel
    - N0 : list/array initial population size
    - P0 : list/array initial phenotypes
    - C0 : float initial nutrient concentration
    - ratefun_pc : vectorized function that returns birth and death *per capita* rates
    - dconcfun : function that gives the derivative of the deterministic variable
    - popkernel_fun : function that returns the new population given the old one and the rates
    - ckernel_fun : deterministic kernel for the resource/continuous variable
    - dt : float time interval
    - tmax : float max time
    - rpars : dict of keyword parameters to pass to the rate function
    - cpars : dict of keyword parameters to pass to the resource rate function
    
    """
    # Evolutive hydbrid discrete birth-death model
    # Simulate evolution of a meta pop using a discrete approx to a BD model
    # 
    # N0 is now a vector
    # P0 is the initial phenotype distribution
    # ratefun_pc needs to be adequately vectorized
    # Mkernel is the mutation kernel, signature N,B,P,**pars --> N',Pp,P'
    N = [N0]
    C = [C0]
    P = [P0]
    time = np.arange(dt,tmax,dt)
    p_parent = P[0]
    for t in time[1:]:
        nvec = N[-1]
        c = C[-1]
        pvec = P[-1] # current phenotype
        b,d = ratefun_pc(nvec=nvec,c=c,x=pvec,pvec=pvec,**rpars) #operates serially on focal phenotype x in pvec, thus B and D are vectors
        B,D = BDpopkernel_fun(nvec,b,d,dt)
        # Population changes due to growth
        nnvec = np.clip(nvec+B-D,0,np.infty)
        # Nutrient concentration change due to population growth
        nc = ckernel_fun(nnvec,nvec,c,pvec,dconcfun,cpars,dt)
        # Changes in the metapopulation due to mutations
        nnvec_prime,p_parent,p_prime = Mkernel(nnvec,B,pvec,**Mpars)
        npvec = p_prime # Will change with mutations
        # Future me might add a skip parameter, as in the code by doulcier
        N.append(nnvec_prime)
        C.append(nc) 
        P.append(npvec)
    return(time,np.array(N),np.array(C),np.array(P))



def evol_hybrid_dbdm(N0,P0,C0,ratefun_pc,dconcfun,BDpopkernel_fun,ckernel_fun,Mkernel,dt,tmax,rpars,cpars,Mpars):
    """
    Modified hybrid_dbdm to accomodate polymorphous metapopulation and a mutation kernel
    - N0 : list/array initial population size
    - P0 : list/array initial phenotypes
    - C0 : float initial nutrient concentration
    - ratefun_pc : vectorized function that returns birth and death *per capita* rates, and a phenotype
    - dconcfun : function that gives the derivative of the deterministic variable
    - popkernel_fun : function that returns the new population given the old one and the rates
    - ckernel_fun : deterministic kernel for the resource/continuous variable
    - dt : float time interval
    - tmax : float max time
    - rpars : dict of keyword parameters to pass to the rate function
    - cpars : dict of keyword parameters to pass to the resource rate function
    
    """
    # Evolutive hydbrid discrete birth-death model
    # Simulate evolution of a meta pop using a discrete approx to a BD model
    # 
    # N0 is now a vector
    # P0 is the initial phenotype distribution
    # ratefun_pc needs to be adequately vectorized
    # Mkernel is the mutation kernel, signature N,B,P,**pars --> N',Pp,P'
    # Now I need to separate phenotype and trait as in there is a developmental phenotype that is function of the trait and time and environmental conditions
    N = [N0] # Metapopulation
    C = [C0] # Concentration
    P = [P0] # Phenotype
    time = np.arange(dt,tmax,dt)
    p_parent = P[0]
    for t in time[1:]:
        nvec = N[-1]
        c = C[-1]
        pvec = P[-1] # current phenotype
        b,d = ratefun_pc(nvec=nvec,c=c,x=pvec,p=pvec,**rpars) #operates serially on focal phenotype x in pvec, thus B and D are vectors
        B,D = BDpopkernel_fun(nvec,b,d,dt)
        # Population changes due to growth
        nnvec = np.clip(nvec+B-D,0,np.infty)
        # Nutrient concentration change due to population growth
        nc = ckernel_fun(nnvec,nvec,c,pvec,dconcfun,cpars,dt)
        # Changes in the metapopulation due to mutations
        nnvec_prime,p_parent,p_prime = Mkernel(nnvec,B,pvec,**Mpars)
        npvec = p_prime # Will change with mutations
        # Future me might add a skip parameter, as in the code by doulcier
        N.append(nnvec_prime)
        C.append(nc) 
        P.append(npvec)
    return(time,np.array(N),np.array(C),np.array(P))


def devo_evo_hybrid(N0,P0,C0,X0,ratefun_pc,dconcfun,BDpopkernel_fun,ckernel_fun,Mkernel,dt,tmax,rpars,cpars,Mpars):
    """
    Modified hybrid_dbdm to accomodate polymorphous metapopulation and a mutation kernel
    - N0 : list/array initial population size
    - P0 : list/array initial phenotypes
    - C0 : float initial nutrient concentration
    - ratefun_pc : vectorized function that returns birth and death *per capita* rates, and a phenotype
    - dconcfun : function that gives the derivative of the deterministic variable
    - popkernel_fun : function that returns the new population given the old one and the rates
    - ckernel_fun : deterministic kernel for the resource/continuous variable
    - dt : float time interval
    - tmax : float max time
    - rpars : dict of keyword parameters to pass to the rate function
    - cpars : dict of keyword parameters to pass to the resource rate function
    
    """
    # Evolutive hydbrid discrete birth-death model
    # Simulate evolution of a meta pop using a discrete approx to a BD model
    # 
    # N0 is now a vector
    # P0 is the initial phenotype distribution
    # ratefun_pc needs to be adequately vectorized
    # Mkernel is the mutation kernel, signature N,B,P,**pars --> N',Pp,P'
    # Now I need to separate phenotype and trait as in there is a developmental phenotype that is function of the trait and time and environmental conditions
    N = [N0] # Metapopulation
    C = [C0] # Concentration
    P = [P0] # Phenotype
    #if X0 is None:
    #    X = [P0]
    #else:
    X = [X0]
    time = np.arange(dt,tmax,dt)
    p_parent = P[0]
    for t in time[1:]:
        nvec = N[-1]
        c = C[-1]
        pvec = P[-1] # current phenotype
        xvec = X[-1]
        b,d,p = ratefun_pc(nvec=nvec,c=c,x=xvec,p=pvec,**rpars) #operates serially on focal phenotype x in pvec, thus B and D are vectors
        # p is now the 'parental' phenotype when it comes to mutations
        
        B,D = BDpopkernel_fun(nvec,b,d,dt)
        # Population changes due to growth
        nnvec = np.clip(nvec+B-D,0,np.infty)
        # Nutrient concentration change due to population growth
        nc = ckernel_fun(nnvec,nvec,c,pvec,dconcfun,cpars,dt)
        # Changes in the metapopulation due to mutations
        nnvec_prime,x_parent,p_parent,x_prime,p_prime = tker.microbial_dvlpt_mutK(nnvec,B,xvec,p,**Mpars) # Should always use microbial_dvlpt_mutK
        npvec = p_prime 
        nxvec = x_primec # Will change with mutations
        # Future me might add a skip parameter, as in the code by doulcier
        N.append(nnvec_prime)
        C.append(nc) 
        X.append(nxvec)
        P.append(npvec)
        
    return(time,np.array(N),np.array(C),np.array(P))


