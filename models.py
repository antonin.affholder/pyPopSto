#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as scint
from scipy import stats
from itertools import chain, zip_longest


#### SOME GENERIC MODELS

# Populations

def logistic_analytic(t,N0,alpha,beta):
    return(beta/(alpha+((beta-alpha*N0)/N0)*np.exp(-beta*t)))

def verhulst_rates_pc(n,beta,alpha):
    return(beta,n*alpha)


deltafun = lambda x: stats.norm.pdf(0, x, 0.1)/50#3.9894228040143269 this is trait based competition 


def PMCstat_poprates_pc(nvec,c,x,pvec,alpha,gamma):
    return(alpha*c/gamma,np.sum(nvec*deltafun(x-pvec)))

PMCstat_prates_vect = np.vectorize(PMCstat_poprates_pc,excluded='x')

# I'd like to permit trait-specific uptake rates

def MCstat_consrate(c,x,alpha):
    # Some basic consumption function
    return(c*np.ones_like(x)*alpha)

def PMCstat_cderiv(nvec,c,pvec,D,C0,alpha,V):
    # Concentration derivative for the substrate
    return(D*(C0-c)-np.sum(MCstat_consrate(c,pvec,alpha)*nvec/V))

#This test of deterministic sim will only work with fixed pvec
def PMCstat_cont_deriv(t,y,pvec,alpha,gamma,D,C0,V):
    # OK I dont remember what this does
    dy = np.zeros_like(y)
    dy[:-1] = [n*(alpha*y[-1]/gamma-np.sum(deltafun(pvec[i]-pvec))) for i,n in enumerate(y[:-1])]
    dy[-1]  = D*(C0-y[-1])-np.sum(MCstat_consrate(c,pvec,alpha)*y[:-1]/V)
    return(dy)


# Nutrients (Chemostat)
def Cstat_poprates_pc(n,c,alpha,gamma,delta):
    return(alpha*c/gamma,delta)

def Cstat_cderiv(n,c,D,C0,alpha,V):
    return(D*(C0-c)-alpha*c*n/V)

def Cstat_cont_deriv(t,y,alpha,gamma,delta,D,C0,V):
    return([y[0]*(alpha*y[1]/gamma-delta),Cstat_cderiv(y[0],y[1],D,C0,alpha,V)])